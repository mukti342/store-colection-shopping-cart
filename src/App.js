import "./App.css";
import Home from "./component/Home";
import Navbar from "./component/Navbar";
import { Route, Routes } from "react-router-dom";
import Products from "./component/Products";
import Product  from "./component/Product";
import Cart from "./component/Cart";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route  path="/" element={<Home/>} />
        <Route  path="/Products" element={<Products/>} />
        <Route  path="/Products/:id" element={<Product/>} />
        <Route path="/Cart/:id" element={<Cart/>}/>
      </Routes>
    </>
  );
}

export default App;
